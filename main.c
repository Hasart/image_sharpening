#include<stdio.h>
#include<stdlib.h>
#include<math.h>

typedef struct {
    unsigned char red, green, blue;
} pixel;

typedef struct {
    int w, h, d;
    pixel *data;
} image;

int main(int argc, char *argv[]) {
    const char *filename = argv[1];
    char buff[8];
    image *img;
    FILE *file = fopen(filename, "rb");
    int temp;


    if (!fgets(buff, sizeof(buff), file)) {
        perror(filename);
        exit(1);
    }


    temp = getc(file);
    if (temp == '#') {
        while (getc(file) != '\n');
        temp = getc(file);
    }
    ungetc(temp, file);
    FILE *file2 = fopen("output.ppm", "wb");
    img = (image *) malloc(sizeof(image));
    temp = fscanf(file, "%d %d", &img->w, &img->h);
    temp = fscanf(file, "%d", &img->d);

    fprintf(file2, "P6\n");
    fprintf(file2, "%d %d\n", img->w, img->h);
    fprintf(file2, "%d\n", img->d);

    while (fgetc(file) != '\n');
    img->data = (pixel *) malloc(sizeof(pixel) * img->w * img->h);


    if (fread(img->data, 3 * img->w, img->h, file) != img->h) {
        fprintf(stderr, "Error!!");
        exit(1);
    }

    fclose(file);



// malloc(sizeof(image));
//    image *res = malloc(sizeof(image));
//    memcpy(res, img, sizeof(image));
//    for (int i = img->w; i < (img->w * img->h) - img->w; i++) { //vynecha prvnai a posledni radek
//        if (i % img->w == 0 ||
//            i % img->w == img->w - 1) { continue; }//vynecha prvnai a posledni prvek v radkupixel px ;
//        temp = (5 * img->data[i].red)
//               - img->data[i - 1].red
//               - img->data[i + 1].red
//               - img->data[i - img->w].red
//               - img->data[i + img->w].red;
//        if (temp < 0) { temp = 0; }
//        else if (temp > 255) { temp = 255; }
//        res->data[i].red = temp;
////        fwrite(&img->data[i].red, 1, 1, file2);
//
//        temp = 5 * img->data[i].green - img->data[i - 1].green - img->data[i + 1].green - img->data[i - img->w].green -
//               img->data[i + img->w].green;
//        if (temp < 0) { temp = 0; }
//        else if (temp > 255) { temp = 255; }
//        res->data[i].green = temp;
////        fwrite(&img->data[i].green, 1, 1, file2);
//
//        temp = 5 * img->data[i].blue - img->data[i - 1].blue - img->data[i + 1].blue - img->data[i - img->w].blue -
//               img->data[i + img->w].blue;
//        if (temp < 0) { temp = 0; }
//        else if (temp > 255) { temp = 255; }
//        res->data[i].blue = temp;
////        fwrite(&img->data[i].blue, 1, 1, file2);
//    }

    int hist0 = 0;
    int hist1 = 0;
    int hist2 = 0;
    int hist3 = 0;
    int hist4 = 0;
    int Y;


//    for (int i = 0; i < (img->w); i++) { //prvni radek
//        fwrite(&img->data[i].red, 1, 1, file2);
//        fwrite(&img->data[i].green, 1, 1, file2);
//        fwrite(&img->data[i].blue, 1, 1, file2);
//
//        Y = round(0.2126 * img->data[i].red + 0.7152 * img->data[i].green + 0.0722 * img->data[i].blue);
//
//        if (Y < 51) {
//            hist0++;
//        } else if (Y < 102) {
//            hist1++;
//        } else if (Y < 153) {
//            hist2++;
//        } else if (Y < 204) {
//            hist3++;
//        } else if (Y < 256) {
//            hist4++;
//        }
//    }
    int tempr, tempg, tempb;

    for (int i = 0; i < (img->w * img->h); i++) {
        if (i < img->w || i >= (img->w * img->h) - img->w || i % img->w == 0 ||
            i % img->w == img->w - 1) {

            Y = round(0.2126 * img->data[i].red + 0.7152 * img->data[i].green + 0.0722 * img->data[i].blue);

            if (Y < 51) {
                hist0++;
            } else if (Y < 102) {
                hist1++;
            } else if (Y < 153) {
                hist2++;
            } else if (Y < 204) {
                hist3++;
            } else if (Y < 256) {
                hist4++;
            }
            fwrite(&img->data[i].red, 1, 1, file2);
            fwrite(&img->data[i].green, 1, 1, file2);
            fwrite(&img->data[i].blue, 1, 1, file2);

            continue;
        }
        tempr = (5 * img->data[i].red)
                - img->data[i - 1].red
                - img->data[i + 1].red
                - img->data[i - img->w].red
                - img->data[i + img->w].red;
        if (tempr < 0) { tempr = 0; }
        else if (tempr > 255) { tempr = 255; }
        fwrite(&tempr, 1, 1, file2);

        tempg = 5 * img->data[i].green - img->data[i - 1].green - img->data[i + 1].green - img->data[i - img->w].green -
                img->data[i + img->w].green;
        if (tempg < 0) { tempg = 0; }
        else if (tempg > 255) { tempg = 255; }
        fwrite(&tempg, 1, 1, file2);

        tempb = 5 * img->data[i].blue - img->data[i - 1].blue - img->data[i + 1].blue - img->data[i - img->w].blue -
                img->data[i + img->w].blue;
        if (tempb < 0) { tempb = 0; }
        else if (tempb > 255) { tempb = 255; }
        fwrite(&tempb, 1, 1, file2);
        Y = (int) round(0.2126 * tempr + 0.7152 * tempg + 0.0722 * tempb);
        if (Y < 51) {
            hist0++;
        } else if (Y < 102) {
            hist1++;
        } else if (Y < 153) {
            hist2++;
        } else if (Y < 204) {
            hist3++;
        } else if (Y < 256) {
            hist4++;
        }
    }
//    for (int i = (img->w * img->h) - img->w; i < (img->w * img->h); i++) { //prvnai
//        fwrite(&img->data[i].red, 1, 1, file2);
//        fwrite(&img->data[i].green, 1, 1, file2);
//        fwrite(&img->data[i].blue, 1, 1, file2);
//
//
//        Y = round(0.2126 * img->data[i].red + 0.7152 * img->data[i].green + 0.0722 * img->data[i].blue);
//        if (Y < 51) {
//            hist0++;
//        } else if (Y < 102) {
//            hist1++;
//        } else if (Y < 153) {
//            hist2++;
//        } else if (Y < 204) {
//            hist3++;
//        } else if (Y < 256) {
//            hist4++;
//        }
//    }
//    fwrite(&img->data[i].blue, 1, 1, fp);

    free(img->data);
    free(img);
//    fwrite(img->data, 3 * img->w, img->h, file2);
    fclose(file2);

//    if (hist0 == 2595618 && hist1 == 2956842 && hist2 == 2273497 && hist3 == 7633196 && hist4 == 466095) {
//        printf("It is OK\n");
//    }
    FILE *file3 = fopen("output.txt", "wb");
    fprintf(file3, "%d %d %d %d %d", hist0, hist1, hist2, hist3, hist4);
    fclose(file3);

}
